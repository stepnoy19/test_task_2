﻿
Процедура ПередЗаписью(Отказ, РежимЗаписи, РежимПроведения)
	
	// Проверка: если все работы выполненные как "Сдано", то установим статус "Выполнено",
	// вопрос пользователю не задаем согласно условию задачи
	
	ПроверитьДокументНаСтатусВыполнено();
	
	СуммаДокумента	= Работы.Итог("Сумма");
	
КонецПроцедуры

Процедура ОбработкаПроведения(Отказ, Режим)
	 
	// Движения делаем только если заявка в статусе "Выполнено"
	//
	Если Статус	<> Перечисления.СтатусыЗаявок.Выполнено Тогда
		Возврат;
	КонецЕсли;
	
	ДвиженияПоРегистрам();
	      	
КонецПроцедуры

Процедура ДвиженияПоРегистрам()
	
	// Немного расплывчатая формулировка по учету работ.
	// Принимаем, что необходимо учитывать только сданные работы, т.к.
	// только за них мы получаем оплату и только их оплачиваем сотрудникам.
	//
	// Одним запросом таблицы для регистров в сгруппированном по соответствующим
	// разрезам, по всей видимости, не получить. Используем 2 запроса.
	
	ДвиженияПоРегиструУслугиПоКонтрагентам();
	
	ДвиженияПоРегиструВыполненныеРаботы();
	
КонецПроцедуры

Процедура ДвиженияПоРегиструУслугиПоКонтрагентам() 	
	
	Запрос			= Новый Запрос;
	Запрос.Текст	=
	"ВЫБРАТЬ
	|	ЗаявкаНаРаботыРаботы.Услуга КАК Номенклатура,
	|	СУММА(ЗаявкаНаРаботыРаботы.Сумма) КАК Сумма
	|ИЗ
	|	Документ.ЗаявкаНаРаботы.Работы КАК ЗаявкаНаРаботыРаботы
	|ГДЕ
	|	ЗаявкаНаРаботыРаботы.Ссылка = &Ссылка
	|	И ЗаявкаНаРаботыРаботы.Сдано
	|
	|СГРУППИРОВАТЬ ПО
	|	ЗаявкаНаРаботыРаботы.Услуга";
	
	Запрос.УстановитьПараметр("Ссылка", Ссылка);
	Выборка	= Запрос.Выполнить().Выбрать();
	
	Движения.УслугиПоКонтрагентам.Записывать	= Истина; 	
	Движения.Хозрасчетный.Записывать			= Истина;
	
	Пока Выборка.Следующий() Цикл
		
		// регистр УслугиПоКонтрагентам
		
		Движение				= Движения.УслугиПоКонтрагентам.Добавить();
		Движение.Период			= Дата;
		Движение.Контрагент		= Контрагент;
		ЗаполнитьЗначенияСвойств(Движение, Выборка);
		
		// регистр бухгалтерии Хозрасчетный  		
		 
		Движение				= Движения.Хозрасчетный.Добавить();
		Движение.Период			= Дата; 
		
		// Дебет 
		Движение.СчетДт = ПланыСчетов.Учебный.РасчетыСПокупателямиИЗаказчиками; 
		Движение.СубконтоДт.Контрагенты = Контрагент; 
				
		// Кредит 
		Движение.СчетКт = ПланыСчетов.Учебный.Продажи; 		
		Движение.СубконтоКт.Номенклатура = Выборка.Номенклатура; 
				
		Движение.Организация = Организация;        		
		
		Движение.Сумма = Выборка.Сумма; 		 
		Движение.Содержание = "Реализация услуг"; 
					
	КонецЦикла;

КонецПроцедуры

Процедура ДвиженияПоРегиструВыполненныеРаботы()
	
	// регистр ВыполненныеРаботы 
	
	Запрос			= Новый Запрос;
	Запрос.Текст	=
	"ВЫБРАТЬ
	|	ЗаявкаНаРаботыРаботы.Услуга КАК Номенклатура,
	|	ЗаявкаНаРаботыРаботы.Сотрудник,
	|	СУММА(ЗаявкаНаРаботыРаботы.Количество * ЗаявкаНаРаботыРаботы.Трудозатраты) КАК Трудозатраты
	|ИЗ
	|	Документ.ЗаявкаНаРаботы.Работы КАК ЗаявкаНаРаботыРаботы
	|ГДЕ
	|	ЗаявкаНаРаботыРаботы.Ссылка = &Ссылка
	|	И ЗаявкаНаРаботыРаботы.Сдано
	|
	|СГРУППИРОВАТЬ ПО
	|	ЗаявкаНаРаботыРаботы.Услуга,
	|	ЗаявкаНаРаботыРаботы.Сотрудник";
	
	Запрос.УстановитьПараметр("Ссылка", Ссылка);
	
	Движения.ВыполненныеРаботы.Записывать = Истина;
	Выборка	= Запрос.Выполнить().Выбрать();
	
	Пока Выборка.Следующий() Цикл
		
		Движение				= Движения.ВыполненныеРаботы.Добавить();
		Движение.Период			= Дата;
		Движение.Организация	= Организация;
		ЗаполнитьЗначенияСвойств(Движение, Выборка);
		
	КонецЦикла;

КонецПроцедуры

#Область СлужебныеПроцедурыИФункции

Процедура ПроверитьДокументНаСтатусВыполнено()
	
	ПараметрыОтбора		= Новый Структура("Сдано", Ложь); 	
	ТабЧастьСОтбором	= Работы.Выгрузить(ПараметрыОтбора, "Сдано");
	
	Если ТабЧастьСОтбором.Количество()=0 Тогда
		
		Статус	=	Перечисления.СтатусыЗаявок.Выполнено;
		
	КонецЕсли;
		
КонецПроцедуры

#КонецОбласти